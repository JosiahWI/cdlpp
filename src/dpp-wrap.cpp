#include <string>
#include <memory>
#include <unordered_map>
#include <discordpp/bot.hh>
#include <discordpp/log.hh>
#include <discordpp/plugin-overload.hh>
#include <discordpp/plugin-ratelimit.hh>
#include <discordpp/rest-beast.hh>
#include <discordpp/websocket-simpleweb.hh>
#include "dpp-wrap.hpp"

template class discordpp::PluginOverload<discordpp::PluginRateLimit<discordpp::WebsocketSimpleWeb<discordpp::RestBeast<discordpp::Bot>>>>;
using dppsrc = discordpp::PluginOverload<discordpp::PluginRateLimit<discordpp::WebsocketSimpleWeb<discordpp::RestBeast<discordpp::Bot>>>>;
static std::unordered_map<void *, std::shared_ptr<dppsrc>> holder;
#define todppsrc(x) (reinterpret_cast<dppsrc*>(x))


Dpp::Dpp() {
    auto srf = std::make_shared<dppsrc>();
    holder[srf.get()] = srf;
    this->src = reinterpret_cast<void*>(srf.get());
    this->handlers = &todppsrc(src)->handlers;
    this->intents = &todppsrc(src)->intents;
}
Dpp::~Dpp() {
    holder.erase(src);
}

void Dpp::initBot(unsigned int apiVersionIn, const std::string &tokenIn,
                  std::shared_ptr<boost::asio::io_context> aiocIn) {
    todppsrc(src)->initBot(apiVersionIn, tokenIn, aiocIn);
}

void Dpp::run() {
    todppsrc(src)->run();
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL) {
    todppsrc(src)->call(requestType, targetURL);
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL, const json &body) {
    todppsrc(src)->call(requestType, targetURL, body);
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL,
               const dpptypes::handleWrite &onWrite) {
    todppsrc(src)->call(requestType, targetURL, onWrite);
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL, const dpptypes::handleRead &onRead) {
    todppsrc(src)->call(requestType, targetURL, onRead);
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL, const json &body,
               const dpptypes::handleWrite &onWrite) {
    todppsrc(src)->call(requestType, targetURL, body, onWrite);
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL, const json &body,
               const dpptypes::handleRead &onRead) {
    todppsrc(src)->call(requestType, targetURL, body, onRead);
}

void Dpp::call(const std::string &requestType,
               const std::string &targetURL, const json &body,
               const dpptypes::handleWrite &onWrite, const dpptypes::handleRead &onRead) {
    todppsrc(src)->call(requestType, targetURL, body, onWrite, onRead);
}

void Dpp::send(const int opcode, const json &payload) {
    todppsrc(src)->send(opcode, payload);
}

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>
#include <fstream>
#include <exception>
#include <filesystem>
#include <memory>
#include <boost/asio.hpp>
#include "bot.hpp"
#include "abstract/message.hpp"
#include "abstract/user.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/env.hpp"
#include "abstract/intents.hpp"
#include "extras.hpp"
#include "cdltypes-incomplete.hpp"

namespace CDL {
std::unordered_map<std::string, cmd> commands;
static std::vector<std::function<void ()>> module_inits;


namespace handlers {
    std::function<void (const std::string& message)> on_error = nullptr;
    std::function<void ()> in_main = nullptr;
    std::function<void (CMessage msg, std::function<void (CMessage)> cb)> on_message = [] (CMessage msg, std::function<void (CMessage)> cb) {
        cb(msg);
    };
    std::function<void (CChannel channel, std::function<void (const std::string&)> cb)> get_prefix = [] (CChannel, std::function<void (const std::string&)> cb) {
        cb("!");
    };
}


void register_command(const std::string& name, cmdfnc function, cmdargs::size_type maxargs) {
    struct cmd thiscmd;
    thiscmd.function = function;
    thiscmd.maxargs = maxargs;
    commands[name] = thiscmd;
}
void deregister_commands(const std::vector<std::string>& names) {
    for (const auto& name : names) {
        commands.erase(name);
    }
}
void register_module_initialiser(std::function<void ()> function) {
    module_inits.push_back(function);
}

void handle_message(CMessage msg) {
    // Check that the message wasn't sent by a bot/webhook
    if (msg->author->bot or msg->webhook) {
        return;
    }
    // Run on message function
    handlers::on_message(msg, [] (CMessage msg) {
        // Check that there are are any commands
        if (commands.empty()) return;
        // Get channel
        msg->get_channel([msg] (CChannel channel) {
            // Determine prefix
            handlers::get_prefix(channel, [msg, channel] (const std::string& prefix) {
                // Cache message
                std::string& content(msg->content);
                // Check if message starts with prefix
                if (content.find(prefix) != 0) {
                  return;
                }
                // Remove prefix
                content.erase(0, prefix.length());
                // Split message
                cmdargs args = extras::strsplit(content, ' ', 1);
                // Check if command exists
                auto func = commands.find(args[0]);
                if (func == commands.end()) {
                    return; // TODO: proper error handling
                }
                // Resplit args
                auto maxargs = func->second.maxargs;
                if (args.size()> 1 and maxargs != 1) {
                    args = extras::strsplit(args[1], ' ', maxargs==0?0:maxargs-1);
                } else {
                    args.erase(args.begin());
                }
                // Run commands function
                func->second.function(msg, channel, args);
            });
        });
    });
}

[[ noreturn ]]
void restart_bot() {
    execv(env.prg_path.c_str(), env.argv);
    perror("Failed to restart");
    std::cerr << "Restarting process automatically may not be supported on your platform; exitting with code 73 instead.\n"
                 "I'd recommend writing a script that handles the restart automatically and opening an issue for your OS ;-)" << std::endl;
    exit(73);
}

void reload_config() {
    env.settings.clear();
    // Read static settings from file
    std::ifstream i("config.json");
    if (not i) {
        std::cerr << "Error: config.json:" << std::strerror(errno) << std::endl;
        abort();
    }
    i>> env.settings;
}

void main(int argc, char **argv, uint16_t intents, std::shared_ptr<boost::asio::io_context> aioc) {
    // Create boost asio context if required
    if (not aioc) {
        if (env.aioc) {
            aioc = env.aioc;
        } else {
            env.aioc = aioc = std::make_shared<asio::io_context>();
        }
    } else {
        if (env.aioc and env.aioc != aioc) {
            std::clog << "WARNING: aioc in env and passed in arg are different and aioc in env not null; using aioc from arg" << std::endl;
        }
        env.aioc = aioc;
    }
    // Store argv and argc in environment
    env.argv = argv;
    env.argc = argc;
#   ifdef __linux__
    env.prg_path = std::filesystem::read_symlink("/proc/self/exe");
#   else
    env.prg_path = env.argv[0];
#   endif
    // Load config if needed
    if (env.settings.empty()) {
        reload_config();
    }
    // Initialise Discord++
    env.bot = std::make_shared<Dpp>();
    bool is_userbot = env.settings.contains("userbot") and bool(env.settings["userbot"]);
    env.bot->initBot(8, (is_userbot?"":"Bot ")+std::string(env.settings["token"]), aioc);
    // Set intents
    *env.bot->intents = intents;
    intents::init();
    // Set default message handlers
    env.bot->handlers->insert(
            {"MESSAGE_CREATE", [](json _data) {
                 // Copy data to cache
                 auto data = new json(_data);
                 // Make sure having cached the channel
                 fetch::channel(s2i((*data)["channel_id"]), [data] (CChannel) {
                     // Get message object
                     auto msg = cache::new_message(*data);
                     // Get member
                     if (data->contains("member")) {
                        msg->member = cache::new_member((*data)["member"], msg->guild_id, msg->author->id, msg->author);
                     }
                     // Clean up
                     delete data;
                     // Handle message
                     handle_message(msg);
                 });
            }}
    );
    env.bot->handlers->insert(
            {"READY", [](json) {
                 // Check if error file can be opened
                 std::ifstream error_file(".error_file.txt");
                 if (error_file.good()) {
                     // Read error message
                     std::string error_message;
                     {
                         std::ostringstream sstr;
                         sstr << error_file.rdbuf();
                         error_message = sstr.str();
                     }
                     // Close and delete file
                     error_file.close();
                     std::filesystem::remove(".error_file.txt");
                     // Run handler
                     if (handlers::on_error) handlers::on_error(error_message);
                 }
            }}
    );
    // Run custom main() code
    if (handlers::in_main) handlers::in_main();
    // Load module initialisers
    for (auto& init_func : module_inits) {
        init_func();
    }
    module_inits.clear();
    // Run bot
#   ifdef NDEBUG
    try {
#   endif
        env.bot->run();
#   ifdef NDEBUG
    } catch (std::exception& e) {
        std::cerr << "The bot has crashed: " << e.what() << "\n"
                     "Writing error file... " << std::flush;
        // Write error file
        std::ofstream error_file(".error_file.txt");
        if (error_file.fail()) {
            std::cerr << std::strerror(errno) << std::endl;
        } else {
            error_file << e.what();
            error_file.close();
            std::cerr << "Done" << std::endl;
        }
        // Restart
        std::cerr << "Restarting..." << std::endl;
        restart_bot();
    }
#   endif
}
};

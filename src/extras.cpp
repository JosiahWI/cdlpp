#include <string>
#include <string_view>
#include <vector>
#include <fstream>
#include <iomanip>
#ifdef __linux__
#    include <sys/sysinfo.h>
#endif
#include "bot.hpp"
#include "abstract/cache.hpp"
#include "abstract/message.hpp"
#include "abstract/member.hpp"
#include "abstract/guild.hpp"
#include "abstract/env.hpp"
#include "extras.hpp"


namespace CDL {
namespace extras {
std::vector<std::string> strsplit(std::string_view s, char delimiter, std::vector<std::string>::size_type times) {
    std::vector<std::string> to_return;
    decltype(s.size()) start = 0, finish = 0;
    while ((finish = s.find_first_of(delimiter, start)) != std::string_view::npos) {
        to_return.emplace_back(s.substr(start, finish - start));
        start = finish + 1;
        if (to_return.size() == times) { break; }
    }
    to_return.emplace_back(s.substr(start));
    return to_return;
}

std::string get_premention(const_CChannel channel, const_CUser author, std::string message) { // TODO: uppercase first character
    std::string pre;
    if (channel->type != ChannelTypes::DM) {
        message[0] = static_cast<char>(std::tolower(*message.begin()));
        pre = author->get_mention()+", ";
    } else {
        message[0] = static_cast<char>(std::toupper(*message.begin()));
    }
    return pre+message;
}

std::string get_avatar_url(uint64_t obj_id, std::string hash) {
    return "https://cdn.discordapp.com/avatars/"+std::to_string(obj_id)+"/"+hash+".png";
}
std::string get_avatar_url(const_CUser user) {
    if (user->avatar.empty()) {
        return "https://cdn.discordapp.com/embed/avatars/"+std::to_string(user->discriminator % 5)+".png";
    } else {
        return "https://cdn.discordapp.com/avatars/"+std::to_string(user->id)+"/"+user->avatar+".png";
    }
}
std::string get_avatar_url(const_CGuild server) {
    if (server->icon_hash.empty()) {
        return "";
    } else {
        return "https://cdn.discordapp.com/icons/"+std::to_string(server->id)+"/"+server->icon_hash+".png";
    }
}

bool is_digits(std::string str, bool maybe_negative) {
    if (str.empty()) return false;
    if (maybe_negative and str[0] == '-') {
        str.erase(0, 1);
    }
    for (const auto& character : str) {
        if (character < '0' or character> '9') {
            return false;
        }
    }
    return true;
}

std::string tolowers(std::string str) {
    for (auto& character : str) {
        if (character>= 'A' and character <= 'Z') {
            character -= 'A' - 'a';
        }
    }
    return str;
}
std::string touppers(std::string str) {
    for (auto& character : str) {
        if (character>= 'a' and character <= 'z') {
            character -= 'a' - 'A';
        }
    }
    return str;
}


std::string url_encode(const std::string &str) {
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (const auto& c : str) {

        // Do not encode stuff that does not need to be encoded
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << std::uppercase
                    << '%' << std::setw(2)
                        << int((unsigned char) c)
                << std::nouppercase;
    }

    return escaped.str();
}

uint64_t find_user_id(CGuild guild, std::string identification) {
    // TODO: Implement proper lookup mechanism
    uint64_t user_id = 0;
    identification = tolowers(identification);
    // Check if it's just an ID
    check_if_id:
    if (is_digits(identification)) {
        user_id = stoul(identification);
        return user_id;
    }
    // Check if is a name
    for (const auto& [member_id, member] : guild->members) {
        auto user = member->lazy_user;
        if (not user) continue;
        // Nickname, username, full name
        auto names = {member->nick, user->username, user->get_full_name()};
        // Check each
        for (auto name : names) {
            // Check if null
            if (name.empty()) continue;
            // Lowercase name
            name = tolowers(name);
            // Compare
            if (name == identification or // Exact?
                name.find(identification) == 0 or // Starts with?
                name.find(identification) != name.npos // Contains?
               ) {
                return user->id;
            }
        }
    }
    // Check if is mention
    if (identification.find("<@") == 0) {
        // Try to get by mention
        identification.erase(0, 2);
        identification.pop_back();
        if (identification[0] == '!') {
            identification.erase(0, 1);
        }
        // Check if what's left is an ID
        goto check_if_id;
    }
    return 0;
}

static std::string *bot_invite_link = nullptr;
const std::string &get_bot_invite_link() {
    if (not bot_invite_link) {
        if (env.settings.contains("botinvite")) {
            bot_invite_link = new std::string(env.settings["botinvite"]);
        } else {
            // TODO
            bot_invite_link = new std::string("https://discordapp.com/api/oauth2/authorize?client_id="+std::to_string(env.self->id)+"&permissions=8&scope=applications.commands%20bot");
        }
    }
    return *bot_invite_link;
}

namespace get_mem { // TODO: This currently works on linux only
#   ifdef __linux__
    size_t used() {
        const std::string identifier = "VmData:\t";
        std::ifstream pstatus("/proc/self/status");
        // Check for success
        if (not pstatus) {
            return 0;
        }
        // Find line
        std::string currline;
        while (currline.find(identifier) != 0) {
            std::getline(pstatus, currline);
        }
        // Close pstatus
        pstatus.close();
        // Get number
        size_t res = 0;
        for (const auto& character : currline) {
            // Check if character is digit
            if (character> '9' or character < '0') {
                continue;
            }
            // Append digit to res
            res += static_cast<size_t>(character - '0');
            res *= 10;
        }
        res /= 10;
        // Return result in MB
        return res / 1000;
    }

    size_t total() {
        // Get sysinfo
        struct sysinfo i;
        sysinfo(&i);
        // Get total ram in MB
        return i.totalram / 1000000;
    }
#   endif
}
}
}

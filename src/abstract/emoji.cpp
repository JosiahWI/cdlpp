#include <string>
#include <nlohmann/json.hpp>
#include "abstract/emoji.hpp"
#include "abstract/guild.hpp"
#include "abstract/permissions.hpp"
#include "abstract/cache.hpp"
#include "abstract/json_f.hpp"
#include "extras.hpp"
#include "cdltypes-incomplete.hpp"


namespace CDL {
Emoji::Emoji(const nlohmann::json& data, Guild *guild) {
    update(data, guild);
}

void Emoji::update(const nlohmann::json& data, Guild *guild) {
    this->guild = guild;
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE("name", name);
        JSON_FPARSE("require_colons", require_colons);
        JSON_FPARSE("managed", managed);
        JSON_FPARSE("animated", animated);
        JSON_FPARSE("available", available);
        JSON_FPARSE_VFNC("user", user, cache::new_user);
        JSON_FPARSE_CUSTOM("roles", jroles, {
                               if (guild) {
                                   for (const auto& [_, role_id] : jroles.items()) {
                                       auto res = guild->roles.find(role_id);
                                       if (res != guild->roles.end()) {
                                           roles.push_back(res->second);
                                       }
                                   }
                               }
                           });
    }
    custom = guild;
}

std::string Emoji::get_url() const {
    return "https://cdn.discordapp.com/emojis/"+std::to_string(id)+".png";
}

std::string Emoji::get_mention() const {
    if (custom) return (animated?"<a:":"<:")+name+":"+std::to_string(id)+">";
    else return name;
}

std::string Emoji::url_encode() const {
    return custom?(extras::url_encode(name)+"%3A"+std::to_string(id)):extras::url_encode(name);
}
};

#include <string>
#include <unordered_map>
#include <nlohmann/json.hpp>
#include "abstract/member.hpp"
#include "abstract/guild.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/env.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"



namespace CDL {
Member::Member(const nlohmann::json& data, uint64_t user_id, CGuild guild, CUser user) {
    update(data, user_id, guild, user);
}

void Member::update(const nlohmann::json& data, uint64_t user_id, CGuild guild, CUser user) {
    this->user_id = user_id;
    this->guild = guild;
    this->lazy_user = user?user:cache::get_user(user_id);
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE("deaf", deaf);
        JSON_FPARSE("mute", mute);
        JSON_FPARSE("nick", nick);
        JSON_FPARSE_CUSTOM("roles", obj, {
                               roles.reserve(obj.size());
                               for (const auto& [_, role_id] : obj.items()) {
                                   roles.push_back(s2i(role_id));
                               }
                           });
    }
    JSON_FPARSE_KEEPCLEAN(data, "nick", nick, "");
}

nlohmann::json Member::dump() const {
    nlohmann::json res;
    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP_CUSTOM("roles", array, obj, {
                              for (const auto& role : roles) {
                                  obj.push_back(std::to_string(role));
                              }
                          });
    }
    return res;
}

void Member::commit(std::function<void (const bool)> cb, CMember source) {
    Member *source_ptr;
    if (source) {
        source_ptr = source.get();
    } else {
        source_ptr = this;
    }
    env.bot->call("PATCH", "/guilds/"+std::to_string(guild->id)+"/members/"+std::to_string(user_id),
                  source_ptr->dump(), [this, cb] (const bool error, nlohmann::json data) {
        if (not error) {
            update(data, user_id, guild, lazy_user);
        }
        if (cb) cb(error);
    });
}

void Member::get_user(std::function<void (CUser)> cb) {
    if (lazy_user) {
        cb(lazy_user);
    } else {
        fetch::user(user_id, [this, cb] (CUser user) {
            lazy_user = user;
            cb(user);
        });
    }
}
void Member::get_user(std::function<void (CUser)> cb) const {
    if (lazy_user) {
        cb(lazy_user);
    } else {
        fetch::user(user_id, [cb] (CUser user) {
            cb(user);
        });
    }
}

void Member::set_nick(const std::string& new_nick, std::function<void (const bool error)> cb) {
    guild->set_nick(user_id, new_nick, cb);
}

void Member::ban(const std::string& reason, uint16_t delete_message_days, std::function<void (const bool error)> cb) {
    guild->ban(user_id, reason, delete_message_days, cb);
}

void Member::kick(const std::string& reason, std::function<void (const bool error)> cb) {
    guild->kick(user_id, reason, cb);
}

void Member::set_voice_state(VoiceState voiceState, std::function<void (const bool)> cb) {
    nlohmann::json sData;
    if (voiceState.channel_id) {
        sData["channel_id"] = std::to_string(voiceState.channel_id);
    }
    if (voiceState.deaf.has_value()) {
        sData["deaf"] = voiceState.deaf.value();
    }
    if (voiceState.mute.has_value()) {
        sData["mute"] = voiceState.mute.value();
    }
    env.bot->call("PATCH", "/guilds/"+std::to_string(guild->id)+"/members/"+std::to_string(user_id),
                  sData, [this, cb] (const bool error, nlohmann::json data) {
        if (not error) {
            update(data, user_id, guild, lazy_user);
        }
        if (cb) cb(error);
    });
}
}

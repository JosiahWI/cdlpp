#include <functional>
#include <vector>
#include <nlohmann/json.hpp>
#include "abstract/intents.hpp"
#include "abstract/channel.hpp"
#include "abstract/guild.hpp"
#include "abstract/role.hpp"
#include "abstract/message.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/env.hpp"
#include "cdltypes-incomplete.hpp"


namespace CDL {
namespace intents {
using json = nlohmann::json;

std::vector<readyHandler> ready;
std::vector<memberHandler> guild_member_add,
                           guild_member_update;
std::vector<memberHandler_r> guild_member_remove;
std::vector<roleHandler> guild_role_create,
                          guild_role_update;
std::vector<roleHandler_r> guild_role_delete;
std::vector<messageHandler> message_create,
                            message_update,
                            message_remove_all_reactions;
std::vector<messageHandler_r> message_delete;
std::vector<channelHandler> channel_create,
                            channel_update;
std::vector<channelHandler_r> channel_delete;
std::vector<emojiHandler> guild_emojis_update;
std::vector<banHandler> guild_ban_add;
std::vector<banHandler_r> guild_ban_remove;
std::vector<guildHandler> guild_create,
                          guild_update,
                          guild_join;
std::vector<guildHandler_r> guild_delete;
std::vector<presenceHandler> presence_update;
std::vector<reactionHandler> message_reaction_add;
std::vector<reactionHandler_r> message_reaction_remove;


static std::vector<uint64_t> initial_guilds;

static void channel_or_msg_delete_handler(const json& data, bool delete_channel) {
    std::unordered_map<uint64_t, CChannel> *channels;
    if (data.contains("guild_id")) {
        auto guild = cache::get_guild(s2i(data["guild_id"]));
        channels = &guild->channels;
    } else {
        channels = &cache::channel_cache;
    }
    auto res = channels->find(s2i(delete_channel?data["id"]:data["channel_id"]));
    if (res != channels->end()) {
        if (delete_channel) {
            for (const auto& fnc : channel_delete) fnc(res->second);
            // Delete channel
            channels->erase(res);
        } else {
            // Delete message
            auto &messages = res->second->messages;
            auto res2 = messages.find(s2i(data["id"]));
            if (res2 != messages.end()) {
                for (const auto& fnc : message_delete) fnc(res2->second);
                messages.erase(res2);
            }
        }
    }
}

void init() {
    // Basic intents
    {
        using namespace discordpp::intents;
        *env.bot->intents |= GUILDS;
    }
    // READY
    env.bot->handlers->insert(
                {"READY", [](json data) {
                     env.self = cache::new_user(data["user"]);
                     for (const auto& [_, uguild] : data["guilds"].items()) {
                         initial_guilds.push_back(s2i(uguild["id"]));
                     }
                     for (const auto& fnc : ready) fnc();
                 }});
    // GUILD_CREATE
    env.bot->handlers->insert(
                {"GUILD_CREATE", [](json data) {
                     auto guild = cache::new_guild(data);
                     for (const auto& [_, member] : data["members"].items()) {
                         auto user = cache::new_user(member["user"]);
                         cache::new_member(member, guild->id, user->id);
                     }
                     for (const auto& [_, channel] : data["channels"].items()) {
                         cache::new_channel(channel, guild->id);
                     }
                     for (const auto& fnc : guild_create) fnc(guild);
                     if (not guild_join.empty()) {
                         bool joined = true;
                         for (const auto& guild_id : initial_guilds) {
                             if (guild_id == guild->id) {
                                 joined = false;
                                 break;
                             }
                         }
                         if (joined) {
                             for (const auto& fnc : guild_join) fnc(guild);
                         }
                     }
                 }});
    // GUILD_UPDATE
    env.bot->handlers->insert(
                {"GUILD_UPDATE", [](json data) {
                     auto guild = cache::new_guild(data);
                     for (const auto& fnc : guild_create) fnc(guild);
                 }});
    // GUILD_DELETE
    env.bot->handlers->insert(
                {"GUILD_DELETE", [](json data) {
                     auto guild_id = s2i(data["id"]);
                     if (not guild_delete.empty()) {
                         auto guild = cache::get_guild(guild_id);
                         if (not guild) {
                             return;
                         }
                         for (const auto& fnc : guild_delete) fnc(guild);
                     }
                     auto res = cache::guild_cache.find(guild_id);
                     if (res != cache::guild_cache.end()) {
                         cache::guild_cache.erase(res);
                     }
                 }});
    // GUILD_ROLE_CREATE
    env.bot->handlers->insert(
                {"GUILD_ROLE_CREATE", [](json data) {
                     auto guild = cache::get_guild(s2i(data["guild_id"]));
                     auto role = new Role(data["role"], guild);
                     guild->roles[role->id] = role;
                     for (const auto& fnc : guild_role_create) fnc(role);
                 }});
    // GUILD_ROLE_UPDATE
    env.bot->handlers->insert(
                {"GUILD_ROLE_UPDATE", [](json data) {
                     auto guild = cache::get_guild(s2i(data["guild_id"]));
                     auto role = new Role(data["role"], guild);
                     guild->roles[role->id] = role;
                     for (const auto& fnc : guild_role_update) fnc(role);
                 }});
    // GUILD_ROLE_DELETE
    env.bot->handlers->insert(
                {"GUILD_ROLE_DELETE", [](json data) {
                     auto &guild_roles = cache::get_guild(s2i(data["guild_id"]))->roles;
                     auto res = guild_roles.find(s2i(data["role_id"]));
                     if (res != guild_roles.end()) {
                         for (const auto& fnc : guild_role_delete) fnc(res->second);
                         delete res->second;
                         guild_roles.erase(res);
                     }
                 }});
    // GUILD_EMOJIS_UPDATE
    env.bot->handlers->insert(
                {"GUILD_EMOJIS_UPDATE", [](json data) {
                     auto guild = cache::get_guild(s2i(data["guild_id"]));
                     if (guild) {
                         guild->clear_emoji_cache();
                         for (const auto& jemoji : data["emojis"]) {
                             cache::new_emoji(jemoji, guild.get());
                         }
                         for (const auto& fnc : guild_emojis_update) fnc(guild);
                     }
                 }});
    // CHANNEL_CREATE
    env.bot->handlers->insert(
                {"CHANNEL_CREATE", [](json data) {
                     auto channel = cache::new_channel(data);
                     for (const auto& fnc : channel_create) fnc(channel);
                 }});
    // CHANNEL_UPDATE
    env.bot->handlers->insert(
                {"CHANNEL_UPDATE", [](json data) {
                     auto channel = cache::new_channel(data);
                     for (const auto& fnc : channel_update) fnc(channel);
                 }});
    // CHANNEL_DELETE
    env.bot->handlers->insert(
                {"CHANNEL_DELETE", [](json data) {
                     channel_or_msg_delete_handler(data, true);
                 }});
    // CHANNEL_PINS_UPDATE
    env.bot->handlers->insert(
                {"CHANNEL_PINS_UPDATE", [](json data) {
                     auto channel = cache::get_channel(s2i(data["channel_id"]));
                     if (channel) {
                         channel->_pins_cache_invalidate();
                     }
                 }});
    // MESSAGE_CREATE
    env.bot->handlers->insert(
                {"MESSAGE_CREATE", [](json _data) {
                     auto data = new json(_data);
                     // Make sure having cached the channel
                     fetch::channel(s2i((*data)["channel_id"]), [data] (CChannel) {
                         auto msg = cache::new_message(*data);
                         delete data;
                         for (const auto& fnc : message_create) fnc(msg);
                     });
                 }});
    // MESSAGE_UPDATE
    env.bot->handlers->insert(
                {"MESSAGE_UPDATE", [](json data) {
                     auto msg = cache::new_message(data);
                     for (const auto& fnc : message_update) fnc(msg);
                 }});
    // MESSAGE_DELETE
    env.bot->handlers->insert(
                {"MESSAGE_DELETE", [](json data) {
                     channel_or_msg_delete_handler(data, false);
                 }});
    // GUILD_MEMBER_ADD
    env.bot->handlers->insert(
                {"GUILD_MEMBER_ADD", [](json data) {
                     auto user = cache::new_user(data["user"]);
                     auto member = cache::new_member(data, s2i(data["guild_id"]), user->id);
                     for (const auto& fnc : guild_member_add) fnc(member);
                 }});
    // GUILD_MEMBER_UPDATE
    env.bot->handlers->insert(
                {"GUILD_MEMBER_UPDATE", [](json data) {
                     auto user = cache::new_user(data["user"]);
                     auto member = cache::new_member(data, s2i(data["guild_id"]), user->id);
                     for (const auto& fnc : guild_member_update) fnc(member);
                 }});
    // GUILD_MEMBER_REMOVE
    env.bot->handlers->insert(
                {"GUILD_MEMBER_REMOVE", [](json data) {
                     auto guild_id = s2i(data["guild_id"]);
                     auto user = cache::new_user(data["user"]);
                     auto &guild_members = cache::get_guild(guild_id)->members;
                     auto res = guild_members.find(user->id);
                     if (res != guild_members.end()) {
                         for (const auto& fnc : guild_member_remove) fnc(res->second);
                         cache::remove_member(guild_id, user->id);
                     }
                 }});
    // GUILD_BAN_ADD
    env.bot->handlers->insert(
                {"GUILD_BAN_ADD", [](json data) {
                     fetch::guild(s2i(data["guild_id"]), [data] (CGuild server) {
                         // Check if ban cache is used for server
                         if (not server->_bans_cache_use) return;
                         // Create entry
                         auto ban = new Ban(data);
                         server->_bans_cache[ban->user->id] = ban;
                         for (const auto& fnc : guild_ban_add) fnc(server, *ban);
                     });
                 }});
    // GUILD_BAN_REMOVE
    env.bot->handlers->insert(
                {"GUILD_BAN_REMOVE", [](json data) {
                     fetch::guild(s2i(data["guild_id"]), [data] (CGuild server) {
                         // Check if ban cache is used for server
                         if (not server->_bans_cache_use) return;
                         // Find ban entry in cache
                         auto res = server->_bans_cache.find(s2i(data["user"]["id"]));
                         if (res != server->_bans_cache.end()) {
                             // Event
                             for (const auto& fnc : guild_ban_remove) fnc(server, *res->second);
                             // Deallocate and erase entry
                             delete res->second;
                             server->_bans_cache.erase(res);
                         } else {
                             // Construct new Ban object on the fly
                             Ban thisban(data);
                             // Event
                             for (const auto& fnc : guild_ban_remove) fnc(server, thisban);
                         }
                     });
                 }});
    // PRESENCE_UPDATE
    env.bot->handlers->insert(
                {"PRESENCE_UPDATE", [](json data) {
                     auto presence = new Presence(data);
                     auto user = presence->user;
                     if (user) {
                         if (user->presence) delete user->presence;
                         user->presence = presence;
                     }
                     for (const auto& fnc : presence_update) fnc(presence);
                 }});
    // MESSAGE_REACTION_ADD
    env.bot->handlers->insert(
                {"MESSAGE_REACTION_ADD", [](json data) {
                     Reaction reaction(data);
                     auto channel = cache::get_channel(reaction.channel_id);
                     if (channel) {
                         auto res = channel->messages.find(reaction.message_id);
                         if (res != channel->messages.end()) {
                             res->second->reactions.push_back(reaction);
                         }
                     }
                     for (const auto& fnc : message_reaction_add) fnc(reaction);
                 }});
    // MESSAGE_REACTION_REMOVE
    env.bot->handlers->insert(
                {"MESSAGE_REACTION_REMOVE", [](json data) {
                     Reaction reaction(data);
                     auto channel = cache::get_channel(reaction.channel_id);
                     if (channel) {
                         auto res = channel->messages.find(reaction.message_id);
                         if (res != channel->messages.end()) {
                             for (auto thisreaction = res->second->reactions.begin(); thisreaction != res->second->reactions.end(); thisreaction++) {
                                 if (*thisreaction == reaction) {
                                     res->second->reactions.erase(thisreaction);
                                 }
                             }
                         }
                     }
                     for (const auto& fnc : message_reaction_remove) fnc(reaction);
                 }});
    // MESSAGE_REACTION_REMOVE_ALL
    env.bot->handlers->insert(
                {"MESSAGE_REACTION_REMOVE_ALL", [](json data) {
                     auto channel = cache::get_channel(data["channel_id"]);
                     if (channel) {
                         auto res = channel->messages.find(data["message_id"]);
                         if (res != channel->messages.end()) {
                             for (const auto& fnc : message_remove_all_reactions) fnc(res->second);
                             res->second->reactions.clear();
                         }
                     }
                 }});
}
}
}

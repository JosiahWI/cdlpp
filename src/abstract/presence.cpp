#include <string>
#include <functional>
#include <initializer_list>
#include <nlohmann/json.hpp>
#include "abstract/presence.hpp"
#include "abstract/env.hpp"
#include "abstract/cache.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"



namespace CDL {
namespace PresenceStatus {
std::initializer_list<std::string> strings = {"online", "dnd", "idle", "invisible", "offline"};
}


Presence::Presence(const nlohmann::json& data) {
    update(data);
}

void Presence::update(const nlohmann::json& data) {
    status = PresenceStatus::offline;
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_VFNC("user", user, cache::new_user);
        JSON_FPARSE_CUSTOM("status", obj, {
                               status = static_cast<PresenceStatus::type>(0);
                               for (const auto& presenceString : PresenceStatus::strings) {
                                   if (obj == presenceString) {
                                       break;
                                   }
                                   (*reinterpret_cast<int*>(&status))++;
                               }
                          });
        JSON_FPARSE_CUSTOM("client_status", obj, {
                               clientStatus = static_cast<ClientStatus::type>(0);
                               for (const auto& clientStatusString : {"desktop", "mobile", "web"}) {
                                   if (obj == clientStatusString) {
                                       break;
                                   }
                                   (*reinterpret_cast<int*>(&clientStatus))++;
                               }
                          });
    }
}

Activity::Activity(std::string text, ActivityType::type type, std::string url) {
    this->text = text;
    this->type = type;
    this->url = url;
}

Activity::Activity(const nlohmann::json& data) {
    update(data);
}

void Activity::update(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE("name", text);
        JSON_FPARSE("type", type);
        JSON_FPARSE("url", url);
    }
}

nlohmann::json Activity::dump() const {
    nlohmann::json fres;
    JSON_FDUMP_BEGIN(fres) {
        JSON_FDUMP("name", text);
        JSON_FDUMP("type", type);
    }
    if (type == ActivityType::streaming) {
        fres["url"] = url;
    }
    return fres;
}

nlohmann::json Presence::dump() const {
    nlohmann::json fres;
    JSON_FDUMP_BEGIN(fres) {
        JSON_FDUMP("status", *(PresenceStatus::strings.begin() + status));
        JSON_FDUMP("afk", status == PresenceStatus::idle);
        JSON_FDUMP_CUSTOM("activities", array, obj, {
                              for (auto& activity : activities) {
                                  if (activity.type != ActivityType::none) {
                                      obj.push_back(activity.dump());
                                  }
                              }
                          });
    }
    if (idle_since == 0) {
        fres["since"] = nullptr;
    } else {
        fres["since"] = idle_since;
    }
    return fres;
}

void Presence::commit() {
    env.bot->send(3, dump());
}

PresenceScroller::PresenceScroller() {
    doScroll();
}

void PresenceScroller::doScroll() {
    auto presences_size = presences.size();
    if (presences_size) {
        if (presence_selector >= presences_size) {
            presence_selector = 0;
        }
        presences[presence_selector++]().commit();
    }
    if (timer) delete timer;
    timer = new boost::asio::steady_timer(*env.aioc, boost::asio::chrono::seconds(speed));
    timer->async_wait([this] (const boost::system::error_code&) {
        doScroll();
    });
}
}

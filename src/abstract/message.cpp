#include <string>
#include <vector>
#include <functional>
#include <nlohmann/json.hpp>
#include "abstract/message.hpp"
#include "abstract/member.hpp"
#include "abstract/emoji.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/env.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"



namespace CDL {
Message::Message(const nlohmann::json& data) {
    update(data);
}

void Message::update(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE_ID("channel_id", channel_id);
        JSON_FPARSE_VFNC("author", author, cache::new_user);
        JSON_FPARSE_ID("guild_id", guild_id);
        JSON_FPARSE_ID("webhook_id", webhook_id);
        JSON_FPARSE("content", content);
        JSON_FPARSE_CUSTOM("mentions", obj, {
                               for (const auto& [_, umember] : obj.items()) {
                                   mentions.push_back(cache::new_user(umember));
                               }
                           });
    }
    webhook = webhook_id;
}

nlohmann::json Message::dump() const {
    nlohmann::json res;
    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP("content", content);
        JSON_FDUMP("embed", embed);
        //JSON_FDUMP("flags", flags);
        //JSON_FDUMP("allowed_mentions", allowed_mentions);
    }
    return res;
}

void Message::commit(std::function<void (const bool)> cb, CMessage source) {
    Message *source_ptr;
    if (source) {
        source_ptr = source.get();
    } else {
        source_ptr = this;
    }
    env.bot->call("PATCH", "/channels/"+std::to_string(channel_id)+"/messages/"+std::to_string(id),
                  source_ptr->dump(), [cb] (const bool error, nlohmann::json data) {
        if (not error) {
            cache::new_message(data["body"]);
        }
        if (cb) cb(error);
    });
}

void Message::get_channel(std::function<void (CChannel)> cb) const {
    fetch::channel(channel_id, cb);
}

void Message::get_guild(std::function<void (CGuild)> cb) const {
    if (not guild_id) {
        cb(nullptr);
    } else {
        fetch::guild(guild_id, cb);
    }
}

CGuild Message::get_guild() const {
    if (not member) {
        return nullptr;
    } else {
        return member->guild;
    }
}

void Message::edit(const std::string& text, std::function<void (CMessage)> cb, const nlohmann::json& embed, uint32_t flags) {
    get_channel([this, text, cb, embed, flags] (CChannel channel) {
        channel->edit_message(id, text, cb, embed, flags);
    });
}

void Message::remove(std::function<void (const bool)> cb) {
    get_channel([this, cb] (CChannel channel) {
        if (cb) {
            channel->delete_message(id, cb);
        } else {
            channel->delete_message(id);
        }
    });
}

void Message::pin(std::function<void (const bool)> cb) {
    env.bot->call("PUT", "/channels/"+std::to_string(channel_id)+"/pins/"+std::to_string(id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}
void Message::unpin(std::function<void (const bool)> cb) {
    env.bot->call("DELETE", "/channels/"+std::to_string(channel_id)+"/pins/"+std::to_string(id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}

void Message::add_reaction(CEmoji emoji, std::function<void (const bool)> cb) {
    env.bot->call("PUT", "/channels/"+std::to_string(channel_id)+"/messages/"+std::to_string(id)+"/reactions/"+emoji->url_encode()+"/@me", [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}
}

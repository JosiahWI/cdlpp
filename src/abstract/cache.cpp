// TODO: make code less terrible
#include <string>
#include <vector>
#include <unordered_map>
#include <nlohmann/json.hpp>
#include "abstract/cache.hpp"
#include "abstract/user.hpp"
#include "abstract/member.hpp"
#include "abstract/guild.hpp"
#include "abstract/channel.hpp"
#include "abstract/message.hpp"
#include "abstract/emoji.hpp"
#include "cdltypes-incomplete.hpp"

namespace CDL {
namespace cache {

std::unordered_map<uint64_t, CChannel> channel_cache;
std::unordered_map<uint64_t, CUser> user_cache;
std::unordered_map<uint64_t, CEmoji> emoji_cache;
std::unordered_map<uint64_t, CGuild> guild_cache;


// User cache
CUser get_user(uint64_t user_id) {
    auto res = user_cache.find(user_id);
    if (res != user_cache.end()) {
        return res->second;
    } else {
        return nullptr;
    }
}

CUser new_user(const nlohmann::json& data) {
    uint64_t user_id = s2i(data["id"]);
    auto res = user_cache.find(user_id);
    if (res != user_cache.end()) {
        res->second->update(data);
        return res->second;
    } else {
        std::shared_ptr<User> new_user = std::make_shared<User>(data);
        if (ctrl::user) user_cache[user_id] = new_user;
        return new_user;
    }
}


// Emoji cache
CEmoji get_emoji(uint64_t emoji_id) {
    auto res = emoji_cache.find(emoji_id);
    if (res != emoji_cache.end()) {
        return res->second;
    } else {
        return nullptr;
    }
}

CEmoji new_emoji(const nlohmann::json& data, Guild *guild) {
    if (data["id"].is_string()) {
        uint64_t emoji_id = s2i(data["id"]);
        auto res = emoji_cache.find(emoji_id);
        if (res != emoji_cache.end()) {
            res->second->update(data, guild);
            return res->second;
        } else {
            auto new_emoji = std::make_shared<Emoji>(data, guild);
            if (ctrl::emoji) {
                emoji_cache[emoji_id] = new_emoji;
                guild->emojis[emoji_id] = new_emoji;
            }
            return new_emoji;
        }
    } else {
        return std::make_shared<Emoji>(data);
    }
}

void remove_emoji(uint64_t emoji_id) {
    auto res = emoji_cache.find(emoji_id);
    if (res != emoji_cache.end()) {
        {
            auto guild_es = res->second->guild->emojis;
            auto res = guild_es.find(emoji_id);
            if (res != guild_es.end()) {
                guild_es.erase(res);
            }
        }
        emoji_cache.erase(res);
    }
}


// Guild/member/channel cache
CGuild new_guild(const nlohmann::json& data) {
    uint64_t guild_id = s2i(data["id"]);
    auto guild = get_guild(guild_id);
    // Check if entry exists
    if (guild) {
        // Update existing entry
        guild->update(data);
        return guild;
    } else {
        guild = std::make_shared<Guild>(data);
        if (ctrl::guild) guild_cache[guild_id] = guild;
        return guild;
    }
}

CGuild get_guild(uint64_t guild_id) {
    auto res = guild_cache.find(guild_id);
    // Check if entry exists
    if (res != guild_cache.end()) {
        // Return resulting object
        return res->second;
    } else {
        // Return error value
        return nullptr;
    }
}

CMember new_member(const nlohmann::json& data, uint64_t guild_id, uint64_t user_id, CUser user) {
    if (ctrl::member) {
        auto guild = get_guild(guild_id);
        // Check if entry exists
        if (guild) {
            // Update existing entry
            auto &members = guild->members;
            // Check if member exists
            auto member_entry = members.find(user_id);
            if (member_entry != members.end()) {
                // Update
                member_entry->second->update(data, user_id, guild, user);
                return member_entry->second;
            } else {
                // Create new
                auto new_member = std::make_shared<Member>(data, user_id, guild, user);
                members[user_id] = new_member;
                return new_member;
            }
        } else {
            // Oops
            throw guildOfMemberNotCachedError();
        }
    } else {
        return std::make_shared<Member>(data, user_id, nullptr, user);
    }
}

void remove_member(uint64_t guild_id, uint64_t user_id) {
    auto guild = get_guild(guild_id);
    // Find guild
    if (guild) {
        auto res = guild->members.find(user_id);
        if (res != guild->members.end()) {
            guild->members.erase(res);
        }
    } else {
        // Oops
        throw guildOfMemberNotCachedError();
    }
}


// Channel/message cache
CChannel new_channel(const nlohmann::json& data, uint64_t guild_id) {
    // Store inside channel/message cache
    uint64_t channel_id = s2i(data["id"]);
    if (ctrl::channel) {
        auto channel = get_channel(channel_id);
        // Check if entry exists
        if (channel) {
            // Update existing entry
            channel->update(data);
        } else {
            // Create new entry
            channel = std::make_shared<Channel>(data);
            if (ctrl::channel) channel_cache[channel_id] = channel;
        }
        // Store inside guild/channel cache
        if (channel->type == ChannelTypes::DM or
            channel->type == ChannelTypes::GROUP_DM) {
            // Channel has no guild, exit
            channel->guild_id = 0;
            return channel;
        }
        if (not channel->guild_id) {
            if (not guild_id) {
                throw guildChannelRequiresGuildIdError();
            }
            channel->guild_id = guild_id;
        } else if (not guild_id) {
            guild_id = channel->guild_id;
        }
        auto guild = get_guild(guild_id);
        if (guild) {
            // Check if channel exists
            auto res = guild->channels.find(channel_id);
            if (res == guild->channels.end()) {
                // New entry
                if (ctrl::channel) guild->channels[channel_id] = channel;
            } else {
                // Update (I don't think the value would ever change anyways... But let's just do it)
                res->second = channel;
            }
        } else {
            // Oops
            throw guildOfChannelNotCachedError();
        }
        return channel;
    } else {
        return std::make_shared<Channel>(data);
    }
}

CChannel get_channel(uint64_t channel_id) {
    auto res = channel_cache.find(channel_id);
    // Check if entry exists
    if (res != channel_cache.end()) {
        // Return resulting object
        return res->second;
    } else {
        // Return error value
        return nullptr;
    }
}

CMessage new_message(const nlohmann::json& data) {
    if (ctrl::message) {
        uint64_t message_id = s2i(data["id"]);
        uint64_t channel_id = s2i(data["channel_id"]);
        CChannel channel;
        // Find guild
        if (data.contains("guild_id")) {
            // Guild known
            auto guild = get_guild(s2i(data["guild_id"]));
            if (guild) {
                // Find channel
                auto res = guild->channels.find(channel_id);
                if (res != guild->channels.end()) {
                    channel = res->second;
                } else {
                    throw channelOfMessageNotCachedError();
                }
            } else {
                throw guildOfChannelNotCachedError();
            }
        } else {
            // Guild unknown
            auto res = channel_cache.find(channel_id);
            if (res != channel_cache.end()) {
                channel = res->second;
            } else {
                throw channelOfMessageNotCachedError();
            }
        }
        // Find message
        auto res = channel->messages.find(message_id);
        // Check if entry exists
        if (res != channel->messages.end()) {
            res->second->update(data);
            return res->second;
        } else {
            auto new_message = std::make_shared<Message>(data);
            channel->messages[message_id] = new_message;
            return new_message;
        }
    } else {
        return std::make_shared<Message>(data);
    }
}


// Exceptions
const char* guildOfMemberNotCachedError::what() const noexcept {return "Tried to cache member of uncached guild";}
const char* guildOfChannelNotCachedError::what() const noexcept {return "Tried to cache channel of uncached guild";}
const char* channelOfMessageNotCachedError::what() const noexcept {return "Tried to cache message of uncached channel";}
const char* guildChannelRequiresGuildIdError::what() const noexcept {return "This channel requires an guild id";}


// Cache control
namespace ctrl {
    bool user = true,
         emoji = true,
         guild = true,
         member = true,
         channel = true,
         message = true;
}
}
}

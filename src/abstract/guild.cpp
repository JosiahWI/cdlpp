#include <string>
#include <vector>
#include <unordered_map>
#include <nlohmann/json.hpp>
#include "extras.hpp"
#include "abstract/guild.hpp"
#include "abstract/role.hpp"
#include "abstract/member.hpp"
#include "abstract/cache.hpp"
#include "abstract/fetch.hpp"
#include "abstract/env.hpp"
#include "abstract/json_f.hpp"
#include "cdltypes-incomplete.hpp"


namespace CDL {
std::string feature_get_nice(const std::string& feature_str) {
    // Copy lowercase string
    auto res = extras::tolowers(feature_str);
    // Remove all _
    for (auto& character : res) {
        if (character == '_') {
            character = ' ';
        }
    }
    return res;
}

Ban::Ban(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE("reason", reason);
        JSON_FPARSE_VFNC("user", user, cache::new_user);
    }
}

Invite::Invite(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE("code", code);
        JSON_FPARSE_VFNC("guild", guild, cache::new_guild);
        JSON_FPARSE_VFNC("channel", channel, cache::new_channel);
        JSON_FPARSE_VFNC("inviter", inviter, cache::new_user);
        JSON_FPARSE_VFNC("target_user", target_user, cache::new_user);
        JSON_FPARSE("target_user_type", target_user_type);
    }
}

Guild::Guild(const nlohmann::json& data) {
    update(data);

    // Enable bans cache if possible
    {
        using namespace discordpp::intents;
        _bans_cache_use = (*env.bot->intents & GUILD_BANS) == GUILD_BANS;
    }
}

void Guild::update(const nlohmann::json& data) {
    JSON_FPARSE_BEGIN(data) {
        JSON_FPARSE_ID("id", id);
        JSON_FPARSE_ID("owner_id", owner_id);
        JSON_FPARSE_ID("afk_channel_id", afk_channel_id);
        JSON_FPARSE_ID("widget_channel_id", widget_channel_id);
        JSON_FPARSE_ID("application_id", application_id);
        JSON_FPARSE_ID("system_channel_id", system_channel_id);
        JSON_FPARSE_ID("rules_channel_id", rules_channel_id);
        JSON_FPARSE_ID("public_updates_channel_id", public_updates_channel_id);
        JSON_FPARSE("name", name);
        JSON_FPARSE("region", region);
        JSON_FPARSE("icon", icon_hash);
        JSON_FPARSE("icon_hash", icon_hash);
        JSON_FPARSE("premium_tier", premium_tier);
        JSON_FPARSE("premium_subscription_count", premium_subscription_count);
        JSON_FPARSE("verification_level", verification_level);
        JSON_FPARSE("splash", splash_hash);
        JSON_FPARSE("discovery_splash", discovery_splash_hash);
        JSON_FPARSE("permissions", my_permissions);
        JSON_FPARSE("afk_timeout", afk_timeout);
        JSON_FPARSE("widget_enabled", widget_enabled);
        JSON_FPARSE("verification_level", verification_level);
        JSON_FPARSE("default_message_notifications", default_message_notifications);
        JSON_FPARSE("explicit_content_filter", explicit_content_filter);
        JSON_FPARSE("mfa_level", mfa_level);
        JSON_FPARSE("system_channel_flags", system_channel_flags);
        JSON_FPARSE("large", large);
        JSON_FPARSE("unavailable", unavailable);
        JSON_FPARSE("member_count", member_count);
        JSON_FPARSE("max_presences", max_presences);
        JSON_FPARSE("member_count", member_count);
        JSON_FPARSE("max_members", max_members);
        JSON_FPARSE("vanity_url_code", vanity_url_code);
        JSON_FPARSE("description", description);
        JSON_FPARSE("banner", banner_hash);
        JSON_FPARSE("preferred_locale", preferred_locale);
        JSON_FPARSE("max_video_channel_users", max_video_channel_users);
        JSON_FPARSE("approximate_member_count", approximate_member_count);
        JSON_FPARSE("approximate_presence_count", approximate_presence_count);
        JSON_FPARSE_CUSTOM("roles", obj, {
                               for (const auto& [role_id, role] : roles) {
                                   delete role;
                               }
                               roles.clear();
                               for (const auto& [_, jrole] : obj.items()) {
                                   auto role = new Role(jrole, id);
                                   roles[role->id] = role;
                               }
                           });
        JSON_FPARSE_CUSTOM("emojis", obj, {
                               clear_emoji_cache();
                               for (const auto& [_, jemoji] : obj.items()) {
                                   cache::new_emoji(jemoji, this);
                               }
                           });
        JSON_FPARSE_CUSTOM("features", obj, {
                               features.clear();
                               for (const auto& [_, feature] : obj.items()) {
                                   features.push_back(feature);
                               }
                           });
    }
    is_owner = owner_id == env.self->id;
}

Guild::~Guild() {
    for (const auto [role_id, role] : roles) {
        delete role;
    }
    clear_emoji_cache();
}

nlohmann::json Guild::dump() const {
    nlohmann::json res;
    JSON_FDUMP_BEGIN(res) {
        JSON_FDUMP_ID("afk_channel_id", afk_channel_id);
        JSON_FDUMP_ID("system_channel_id", system_channel_id);
        JSON_FDUMP("name", name);
        JSON_FDUMP("region", region);
        JSON_FDUMP("verification_level", verification_level);
        JSON_FDUMP("default_message_notifications", default_message_notifications);
        JSON_FDUMP("explicit_content_filter", explicit_content_filter);
        JSON_FDUMP("afk_timeout", afk_timeout);
        JSON_FDUMP_CUSTOM("roles", array, jroles, {
                                for (const auto& [role_id, role] : roles) {
                                      auto jrole = role->dump(true);
                                      jrole["id"] = std::to_string(role->id);
                                      jroles.push_back(jrole);
                                }
                          });
        JSON_FDUMP_CUSTOM("channels", array, jchannels, {
                              for (int8_t categories = 1; categories != -1; categories--) {
                                  for (const auto& [channel_id, channel] : channels) {
                                      if ((channel->type == ChannelTypes::GUILD_CATEGORY) != categories) continue;
                                      auto jchannel = channel->dump();
                                      jchannel["id"] = std::to_string(channel->id);
                                      jchannels.push_back(jchannel);
                                  }
                              }
                          });
    }
    if (is_owner) {
        res["owner_id"] = std::to_string(owner_id);
    }
    return res;
}

void Guild::commit(std::function<void (const bool)> cb, CGuild source) {
    Guild *source_ptr;
    if (source) {
        source_ptr = source.get();
    } else {
        source_ptr = this;
    }
    env.bot->call("PATCH", "/guilds/"+std::to_string(id), source_ptr->dump(), [cb] (const bool error, nlohmann::json data) {
        if (not error) {
            cache::new_guild(data["body"]);
        }
        if (cb) cb(error);
    });
}

void Guild::create(const nlohmann::json& src, std::function<void (CGuild)> cb) {
    env.bot->call("POST", "/guilds", src, [cb] (const bool error, nlohmann::json data) {
        CGuild fres = nullptr;
        if (not error) {
            fres = cache::new_guild(data["body"]);
        }
        if (cb) {
            cb(fres);
        }
    });
}
void Guild::create(Guild& src, std::function<void (CGuild)> cb) {
    create(src.dump(), cb);
}

void Guild::remove(std::function<void (const bool)> cb) {
    env.bot->call("DELETE", "/guilds/"+std::to_string(id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}


void Guild::clear_emoji_cache() {
    while (not emojis.empty()) {
        auto res = emojis.begin();
        auto emoji_id = res->first;
        emojis.erase(res);
        cache::remove_emoji(emoji_id);
    }
}

void Guild::get_owner(std::function<void (CUser)> cb) {
    fetch::user(owner_id, cb);
}

std::string Guild::get_icon_url() const {
    return extras::get_avatar_url(shared_from_this());
}

bool Guild::has_perm(uint64_t uor_id, bool is_role, Permissions::type permission) const {
    // Try to find role
    if (is_role) {
        auto res = roles.find(uor_id);
        if (res != roles.end()) {
            auto permissions = res->second->permissions;
            if ((permissions & permission) == permission or
                    (permissions & Permissions::ADMINISTRATOR) == Permissions::ADMINISTRATOR) {
                return true;
            } else {
                return false;
            }
        }
    }
    // Check user
    else {
        // Always true if server owner
        if (owner_id == uor_id) {
            return true;
        }
        // Try to find roles of user
        auto res = members.find(uor_id);
        if (res != members.end()) {
            for (const auto& role_id : res->second->roles) {
                if (has_perm(role_id, true, permission)) {
                    return true;
                }
            }
        }
    }
    // Assume false
    return false;
}
bool Guild::has_perm(CUser user, Permissions::type permission) const {
    return has_perm(user->id, false, permission);
}
bool Guild::has_perm(Role *role, Permissions::type permission) const {
    return has_perm(role->id, true, permission);
}

std::vector<Role*> Guild::get_roles(uint64_t user_id) const {
    std::vector<Role*> fres;
    auto res = members.find(user_id);
    if (res != members.end()) {
        for (const auto& role_id : res->second->roles) {
            auto res = roles.find(role_id);
            if (res != roles.end()) {
                fres.push_back(res->second);
            }
        }
    }
    return fres;
}
std::vector<Role*> Guild::get_roles(const_CUser user) const {
    return get_roles(user->id);
}

void Guild::get_bans(std::function<void (const std::unordered_map<uint64_t, Ban*>&)> cb) {
    if (_bans_cache_use) {
        cb(_bans_cache);
    } else {
        env.bot->call("GET", "/guilds/"+std::to_string(id)+"/bans", [this, cb] (const bool error, const nlohmann::json resp) {
            if (error) {
                // Return empty list (TODO: handle error properly)
                cb({});
                return;
            }
            for (const auto& [_, ban] : resp["body"].items()) {
                auto thisban = new Ban(ban);
                _bans_cache[thisban->user->id] = thisban;
            }
            cb(_bans_cache);
        });
    }
}

void Guild::ban(uint64_t user_id, const std::string& reason, uint16_t delete_message_days, std::function<void (const bool error)> cb) {
    env.bot->call("PUT", "/guilds/"+std::to_string(id)+"/bans/"+std::to_string(user_id), {
                      {"delete_message_days", delete_message_days},
                      {"reason", reason}
                  }, [cb] (const bool error, nlohmann::json) {
        if (cb) {
            cb(error);
        }
    });
}
void Guild::ban(CUser user, const std::string& reason, uint16_t delete_message_days, std::function<void (const bool error)> cb) {
    ban(user->id, reason, delete_message_days, cb);
}

void Guild::kick(uint64_t user_id, const std::string& reason, std::function<void (const bool error)> cb) {
    env.bot->call("DELETE", "/guilds/"+std::to_string(id)+"/members/"+std::to_string(user_id), {
                      {"reason", reason}
                  }, [cb] (const bool error, nlohmann::json) {
        if (cb) {
            cb(error);
        }
    });
}
void Guild::kick(CUser user, const std::string& reason, std::function<void (const bool error)> cb) {
    kick(user->id, reason, cb);
}

void Guild::unban(uint64_t user_id, std::function<void (const bool error)> cb) {
    env.bot->call("DELETE", "/guilds/"+std::to_string(id)+"/bans/"+std::to_string(user_id),
                  [cb] (const bool error, nlohmann::json) {
        if (cb) {
            cb(error);
        }
    });
}
void Guild::unban(CUser user, std::function<void (const bool error)> cb) {
    unban(user->id, cb);
}

uint64_t Guild::get_special_channel(specialChannel::type special_channel_type) const {
    switch(special_channel_type) {
        using namespace specialChannel;
        case afk: return afk_channel_id;
        case widget: return widget_channel_id;
        case specialChannel::system: return system_channel_id;
        case rules: return rules_channel_id;
        case public_updates: return public_updates_channel_id;
    }
    return 0;
}
void Guild::get_special_channel(specialChannel::type special_channel_type, std::function<void (CChannel)> cb) {
    auto channel_id = get_special_channel(special_channel_type);
    if (channel_id) {
        fetch::channel(channel_id, cb, shared_from_this());
    } else {
        cb(nullptr);
    }
}

void Guild::create_channel(const nlohmann::json& channel, std::function<void (CChannel)> cb) {
    env.bot->call("POST", "/guilds/"+std::to_string(id)+"/channels", channel, [this, cb] (const bool error, const nlohmann::json data) {
        CChannel channel = nullptr;
        if (not error) {
            channel = cache::new_channel(data["body"], id);
        }
        if (cb) cb(channel);
    });
}
void Guild::create_channel(Channel& channel, std::function<void (CChannel)> cb) {
    create_channel(channel.dump(), cb);
}

void Guild::create_role(const nlohmann::json& role, std::function<void (Role*)> cb) {
    env.bot->call("POST", "/guilds/"+std::to_string(id)+"/roles", role, [this, cb] (const bool error, const nlohmann::json data) {
        Role *role = nullptr;
        if (not error) {
            role = new Role(data["body"], id);
            roles[role->id] = role;
        }
        if (cb) cb(role);
    });
}
void Guild::create_role(Role *role, std::function<void (Role*)> cb) {
    create_role(role->dump(), cb);
}

void Guild::move_what(uint64_t obj_id, const std::string& ep, int32_t position, std::function<void (const bool error)> cb) {
    env.bot->call("PATCH", "/guilds/"+std::to_string(id)+ep, {
                      {"id", std::to_string(obj_id)},
                      {"position", position}
                  }, [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}

void Guild::move_channel(uint64_t channel_id, int32_t position, std::function<void (const bool error)> cb) {
    move_what(channel_id, "/channels", position, cb);
}
void Guild::move_channel(CChannel channel, int32_t position, std::function<void (const bool error)> cb) {
    move_channel(channel->id, position, cb);
}

void Guild::move_role(uint64_t role_id, int32_t position, std::function<void (const bool error)> cb) {
    move_what(role_id, "/roles", position, cb);
}
void Guild::move_role(Role *role, int32_t position, std::function<void (const bool error)> cb) {
    move_channel(role->id, position, cb);
}

void Guild::remove_channel(uint64_t channel_id, std::function<void (const bool error)> cb) {
    Channel incomplete_channel({});
    incomplete_channel.id = channel_id;
    incomplete_channel.remove(cb);
}
void Guild::remove_channel(CChannel channel, std::function<void (const bool error)> cb) {
    channel->remove(cb);
}

void Guild::remove_role(uint64_t role_id, std::function<void (const bool error)> cb) {
    env.bot->call("DELETE", "/guilds/"+std::to_string(id)+"/roles/"+std::to_string(role_id), [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}
void Guild::remove_role(Role *role, std::function<void (const bool error)> cb) {
    remove_channel(role->id, cb);
}

void Guild::set_nick(uint64_t user_id, const std::string& new_nick, std::function<void (const bool error)> cb) {
    // Get route
    std::string route = "/guilds/"+std::to_string(id)+"/members/";
    if (user_id == env.self->id) {
        route.append("@me/nick");
    } else {
        route.append(std::to_string(user_id));
    }
    env.bot->call("PATCH", route, {
                      {"nick", new_nick}
                  }, [cb] (const bool error, nlohmann::json) {
        if (cb) cb(error);
    });
}
void Guild::set_nick(CMember member, const std::string& new_nick, std::function<void (const bool error)> cb) {
    set_nick(member->user_id, new_nick, cb);
}
}

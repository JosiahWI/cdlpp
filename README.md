# CDL++
[<img src="joindiscord.png" alt="Join the Discord server" width="300" height="92.5"/>](https://discord.gg/SUYhncUmG4)

## Name
**C++** **D**iscord **L**ibrary<br>
Short, *CDL* or *cdlpp*

## What works? What doesn't work?
Works:

 * Basically everything that most bot developers would need.

Doesn't work:

 * Reactions (except for intents)
 * Determine cause of errors in callbacks

## Project template
If you don't want to read through the tutorial, I recommend your bot on this project template: https://gitlab.com/niansa/cdl-template.git

In case you don't target Windows I'd recommend using this template instead: https://gitlab.com/niansa/cdl-template-dynamic.git since it allows for dynamic module loading and uses a system-wide installed version of CDL++ instead of submoduling it.

## Small tutorial

### Adding the submodule
First, make sure your project directory is already a git repository:

 * `git init`

Now that it is a Git repository, proceed by adding CDL++ as a submodule:

 * `mkdir -p lib && cd lib`
 * `git submodule add https://gitlab.com/niansa/cdlpp.git`
 * `git submodule update --init --recursive ---depth 1`

### Edit your cmake file
Okay, now that we have a local copy, we need to add CDL++ to your CMakeLists.txt. To "import" CDL++, add the following lines under your `project()` call:

    include(lib/cdlpp/use.cmake)
    cdlpp(lib/cdlpp)
    cdlpp_libs()

The last thing that needs to be done is to add the source files and header paths of CDL++. It is sufficient to add `${CDLPP_SRC_FILES}` to your `add_executable()` call and `${CDLPP_INCLUDE_DIRS}` to your `target_include_directories()` call.

Here is an example of what your CMakeLists.txt might look like now: https://gitlab.com/niansa/cdlpp/-/snippets/2054384

### Create configuration file
CDL++ reads tokens and database credentials from a file called `config.json`. Create the following file in your project root and enter your credentials (we will not use a database yet):

    {
        "token": "<your bot token>"
    }

Then, to automatically copy the configuration to the build directory, add this line to the end of your CMakeLists.txt:

    configure_file(config.json ./ COPYONLY)

### Let's get into the real thing!
Cool! We can now program our first command handler!
Let's start with a file that contains this:

    #include <string>
    #include "bot.hpp"
    #include "cdltypes.hpp"
    using namespace CDL;
    
    int main(int argc, char **argv) {
        main(argc, argv);
    }

And be sure to add it to your CMakeLists.txt.

Our bot should now be ready to be launched for the first time.

Check to see if the bot has come online. It doesn't have any commands or intent handlers yet.
We now add a sample command by defining a function like this:

    void echo(CMessage msg, CChannel channel, cmdargs& args) {
        channel->send("echo from "+msg->author->username+": "+args[0]);
    }

And make sure you register it (as an example in your `main()`):

    register_command("echo", echo, 1);

The first argument of register_command is the name, the second is a lambda or function that acts as a handler. The third argument sets the maximum number of arguments (with the value `3` the argument string `some_id 2y is a spammer` evaluates to `{"some_id", "2y", "is a spammer"}`). It also accepts `NO_ARGS` and `INF_ARGS`.

We also need to set the intents since CDL++ uses API v8. Add the list of intents to your call to `CDL::main()`:

    using namespace intent_vals;
    CDL::main(argc, argv, GUILD_MESSAGES | DIRECT_MESSAGES);

Now that we have created a command, we can build and restart our bot and type:

    !echo Hello world!

and the bot should respond!

But how do we set our own prefix? Well, by defining a prefix getter. Add:

    handlers::get_prefix = [] (CChannel channel, auto cb) {
        cb("c-");
    };

into your `main()` function and your prefix will be `c-`. Isn't that easy? Well, CDL++ is at a fairly early stage of development. That means it will become even easier and more flexible in the future!


## Special thanks
First of all, I would like to thank [UndarkAido](https://github.com/UndarkAido) for developing [Discord++](https://github.com/DiscordPP/discordpp), the lower-level Discord library on which CDL++ is based.

Next, I would like to thank [zeroxs](https://github.com/zeroxs) for developing [aegis.cpp](https://github.com/zeroxs/aegis.cpp), another semi-high level Discord library in C++. It is the library on which I started developing CDL++.

This may sound strange, but I also want to thank all the developers of [discord.py](https://github.com/Rapptz/discord.py) because it's the library that made me learn Discord bots when I was still a Python developer.

I definitely should also not forget to thank [Mika314](https://github.com/mika314) because he reviewed some parts of CDL++ and gave me some really helpful suggestions.

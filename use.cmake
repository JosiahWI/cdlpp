cmake_minimum_required(VERSION 3.5)


function(cdlpp cdlpp_dir)
    SET(CDLPP_INCLUDE_DIRS
        ${discordpp_SOURCE_DIR}
        ${discordpp-rest-beast_SOURCE_DIR}
        ${discordpp-websocket-simpleweb_SOURCE_DIR}
        ${discordpp-plugin-overload_SOURCE_DIR}
        ${discordpp-plugin-ratelimit_SOURCE_DIR}
        ${cdlpp_dir}/include/
    PARENT_SCOPE)
        
    add_subdirectory(${cdlpp_dir}/lib/discordpp)
    add_subdirectory(${cdlpp_dir}/lib/rest-beast)
    add_subdirectory(${cdlpp_dir}/lib/websocket-simpleweb)
    add_subdirectory(${cdlpp_dir}/lib/plugin-overload)
    add_subdirectory(${cdlpp_dir}/lib/plugin-ratelimit)

    set(CDLPP_SRC_FILES
        ${cdlpp_dir}/src/bot.cpp
        ${cdlpp_dir}/src/dpp-wrap.cpp
        ${cdlpp_dir}/src/extras.cpp
    
        ${cdlpp_dir}/src/abstract/intents.cpp
        ${cdlpp_dir}/src/abstract/fetch.cpp
        ${cdlpp_dir}/src/abstract/cache.cpp
        ${cdlpp_dir}/src/abstract/env.cpp
        ${cdlpp_dir}/src/abstract/channel.cpp
        ${cdlpp_dir}/src/abstract/guild.cpp
        ${cdlpp_dir}/src/abstract/message.cpp
        ${cdlpp_dir}/src/abstract/role.cpp
        ${cdlpp_dir}/src/abstract/user.cpp
        ${cdlpp_dir}/src/abstract/member.cpp
        ${cdlpp_dir}/src/abstract/emoji.cpp
        ${cdlpp_dir}/src/abstract/presence.cpp
        ${cdlpp_dir}/src/abstract/reaction.cpp
    PARENT_SCOPE)
endfunction()

function(cdlpp_libs)
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(format REQUIRED IMPORTED_TARGET fmt)
    
    target_link_libraries(${PROJECT_NAME} PUBLIC
            PkgConfig::format)
    target_link_libraries(${PROJECT_NAME} PUBLIC
            "crypto")
    target_link_libraries(${PROJECT_NAME} PUBLIC
            "ssl")
    target_link_libraries(${PROJECT_NAME} PUBLIC
            "pthread")
    target_link_libraries(${PROJECT_NAME} PUBLIC
            "stdc++fs")
endfunction()

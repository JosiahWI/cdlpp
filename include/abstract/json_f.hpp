#pragma once
#include <string>
#include <nlohmann/json.hpp>
#include "s2i.hpp"

inline void JSON_F_NOOP() {}


#define JSON_FPARSE_BEGIN(data) for (const auto& [jdobj___key, jdobj___value]: data.items())
#define JSON_FPARSE_IFKEY(ekey) if (ekey == jdobj___key && not jdobj___value.is_null())
#define JSON_FPARSE_VFNC(ekey, tvar, vfnc) JSON_FPARSE_IFKEY(ekey) {(tvar) = (vfnc(jdobj___value)); continue;}JSON_F_NOOP()
#define JSON_FPARSE_ID(ekey, tvar) JSON_FPARSE_VFNC(ekey, tvar, s2i)
#define JSON_FPARSE(ekey, tvar) JSON_FPARSE_VFNC(ekey, tvar, std::move)
#define JSON_FPARSE_CUSTOM(ekey, obj_name, ...) JSON_FPARSE_IFKEY(ekey) {auto &obj_name = jdobj___value; {__VA_ARGS__}; continue;}JSON_F_NOOP()
#define JSON_FPARSE_KEEPCLEAN(data, ekey, tvar, def) if (not data.contains(ekey) or data[ekey].is_null()) tvar = def

#define JSON_FDUMP_BEGIN(output) nlohmann::json& jdobj___ = output;
#define JSON_FDUMP(ekey, tvar) jdobj___[ekey] = (tvar)
#define JSON_FDUMP_VFNC(ekey, tvar, vfnc) JSON_FDUMP(ekey, vfnc(tvar))
#define JSON_FDUMP_ID(ekey, tvar) if (tvar != 0) {JSON_FDUMP_VFNC(ekey, tvar, std::to_string);} else {JSON_FDUMP(ekey, nullptr);}JSON_F_NOOP()
#define JSON_FDUMP_CUSTOM(ekey, obj_type, obj_name, ...) {jdobj___[ekey] = nlohmann::json::obj_type(); auto &obj_name = jdobj___[ekey]; {__VA_ARGS__}}JSON_F_NOOP()

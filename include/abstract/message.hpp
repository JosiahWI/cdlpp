#pragma once
namespace CDL {
struct Mention;
class Message;
}

#include <string>
#include <vector>
#include <functional>
#include <nlohmann/json.hpp>
#include "channel.hpp"
#include "reaction.hpp"
#include "../cdltypes-incomplete.hpp"


namespace CDL {
// https://discord.com/developers/docs/resources/channel#message-object
class Message : public std::enable_shared_from_this<Message> {
public:
    std::string content;
    uint64_t id,
             channel_id,
             guild_id = 0,
             webhook_id = 0;
    bool webhook;
    CUser author;
    CMember member = nullptr;
    std::vector<CUser> mentions;
    std::vector<Reaction> reactions;
    nlohmann::json embed;

    Message(const nlohmann::json& data);
    nlohmann::json dump() const;
    void commit(std::function<void (const bool)> cb = nullptr, CMessage source = nullptr);
    void update(const nlohmann::json& data);
    void get_channel(std::function<void (CChannel)> cb) const;
    void get_guild(std::function<void (CGuild)> cb) const;
    CGuild get_guild() const;
    void edit(const std::string& text, std::function<void (CMessage)> cb = nullptr, const nlohmann::json& embed = nullptr, uint32_t flags = 0);
    void remove(std::function<void (const bool)> cb = nullptr);
    void pin(std::function<void (const bool)> cb = nullptr);
    void unpin(std::function<void (const bool)> cb = nullptr);
    void add_reaction(CEmoji emoji, std::function<void (const bool)> cb = nullptr);

    template<typename... Args>
    void reply(Args&&...args) {
        auto sharedthis = shared_from_this();
        get_channel([sharedthis, args...] (CChannel channel) {
            channel->_in_reply_to = sharedthis;
            channel->send(args...);
            channel->_in_reply_to = nullptr;
        });
    }
};
}

#pragma once
namespace CDL {
class Reaction;
}

#include <string>
#include <nlohmann/json.hpp>
#include "../cdltypes-incomplete.hpp"



namespace CDL {
// https://discord.com/developers/docs/topics/gateway#message-reaction-add-message-reaction-add-event-fields
class Reaction {
    nlohmann::json _emoji_raw;
    CEmoji _emoji = nullptr;

public:
    uint64_t user_id,
             channel_id,
             message_id,
             guild_id = 0;
    CMember member = nullptr;

    Reaction(const nlohmann::json& data);
    bool operator ==(const Reaction&);
    void get_emoji(std::function<void (CEmoji)> cb);
    void get_emoji(std::function<void (CEmoji)> cb) const;
    void get_user(std::function<void (CUser)> cb) const;
    void get_channel(std::function<void (CChannel)> cb) const;
    void get_guild(std::function<void (CGuild)> cb) const;
};
}

#pragma once
#include "user.hpp"
#include "../cdltypes-incomplete.hpp"



namespace CDL {
namespace fetch {
void message(uint64_t id, std::function<void (CMessage)> cb, const_CChannel channel);
void channel(uint64_t id, std::function<void (CChannel)> cb, const_CGuild guild = nullptr);
void guild(uint64_t id, std::function<void (CGuild)> cb);
void user(uint64_t id, std::function<void (CUser)> cb);
}
}

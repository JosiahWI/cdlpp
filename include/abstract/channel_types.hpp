#pragma once


namespace CDL {
// https://discord.com/developers/docs/resources/channel#channel-object-channel-types
namespace ChannelTypes {
enum type {
    GUILD_TEXT,
    DM,
    GUILD_VOICE,
    GROUP_DM,
    GUILD_CATEGORY,
    GUILD_NEWS,
    GUILD_STORE
};
}
}

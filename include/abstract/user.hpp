#pragma once
namespace CDL {
class User;
}

#include <string>
#include <nlohmann/json.hpp>
#include "../cdltypes-incomplete.hpp"



namespace CDL {
// https://discord.com/developers/docs/resources/user#user-object
class User : public std::enable_shared_from_this<User> {
public:
    uint64_t id;
    std::string username,
                avatar;
    bool system = false,
         bot = false,
         mfa_enabled = false;
    int discriminator;
    Presence *presence = nullptr;

    User(const nlohmann::json& userdata);
    ~User();
    void update(const nlohmann::json& data);
    std::string get_full_name() const;
    __attribute__ ((pure)) std::string get_mention() const;
    std::string get_avatar_url();
    CMember get_member(CGuild guild) const;
    CMember get_member(uint64_t guild_id) const;
    void get_dm(std::function<void (CChannel )> cb);
};
}

#pragma once
namespace CDL {
struct Emoji;
}

#include <string>
#include <nlohmann/json.hpp>
#include "../cdltypes-incomplete.hpp"


namespace CDL {
// https://discord.com/developers/docs/resources/emoji
class Emoji {
public:
    uint64_t id;
    std::string name;
    std::vector<Role*> roles;
    CUser user = nullptr;
    Guild *guild;
    bool require_colons = true,
         managed = false,
         animated = false,
         available = true,
         custom;

    Emoji(const nlohmann::json& data, Guild *guild = nullptr);
    void update(const nlohmann::json& data, Guild *guild = nullptr);
    __attribute__ ((pure)) std::string get_url() const;
   __attribute__ ((pure)) std::string get_mention() const;
   __attribute__ ((pure)) std::string url_encode() const;
};
}

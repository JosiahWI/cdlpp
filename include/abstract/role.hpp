#pragma once
namespace CDL {
struct RoleTags;
class Role;
}

#include <cstdint>
#include <string>
#include <nlohmann/json.hpp>
#include "permissions.hpp"
#include "../cdltypes-incomplete.hpp"



namespace CDL {
// https://discord.com/developers/docs/topics/permissions#role-object-role-tags-structure
struct RoleTags {
    uint64_t bot_id = 0;
    uint64_t integration_id = 0;
    bool premium_subscriber = false;
};

// https://discord.com/developers/docs/topics/permissions#role-object
class Role {
public:
    uint64_t id,
             guild_id;
    std::string name = "new role";
    uint32_t color = 0;
    int32_t position;
    Permissions::type permissions = Permissions::NONE;
    bool hoist = false,
         managed,
         mentionable = false;
    RoleTags tags;
    CGuild lazy_guild = nullptr;

    Role(const nlohmann::json& data, uint64_t guild_id);
    Role(const nlohmann::json& data, CGuild guild = nullptr);
    void update(const nlohmann::json& data, uint64_t guild_id);
    void update(const nlohmann::json& data, CGuild guild);
    nlohmann::json dump(bool position = false) const;
    void commit(std::function<void (const bool)> cb, Role *source = nullptr);
    void get_guild(std::function<void (CGuild)> cb);
    void get_guild(std::function<void (CGuild)> cb) const;
    void remove(std::function<void (const bool)> cb);
    __attribute__ ((pure)) std::string get_mention() const;
};
}

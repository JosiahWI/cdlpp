#pragma once
namespace CDL {
class Channel;
}

#include <string>
#include <vector>
#include <optional>
#include <nlohmann/json.hpp>
#include "channel_types.hpp"
#include "permissions.hpp"
#include "../cdltypes-incomplete.hpp"



namespace CDL {
// https://discord.com/developers/docs/resources/channel#overwrite-object
struct PermissionOverwrite {
    uint64_t id; // role or user id
    enum Types {
        role,
        member
    };
    bool type; // ether 0 (role) or 1 (member)
    // Are we wasting 32 bits?
    uint64_t allow;
    uint64_t deny;
};

// https://discord.com/developers/docs/resources/channel#channel-object
class Channel {
    std::unordered_map<uint64_t, CMessage> *_pins_cache = nullptr;
public:
    std::string name,
                topic;
    uint64_t id,
             parent_id = 0,
             guild_id = 0,
             last_message_id = 0;
    int32_t position = 0,
            user_limit = 0,
            rate_limit_per_user = 0,
            bitrate = 64000;
    ChannelTypes::type type = ChannelTypes::GUILD_TEXT;
    bool nsfw = false;
    std::unordered_map<uint64_t, CMessage> messages;
    std::unordered_map<uint64_t, PermissionOverwrite> overwrites;
    CMessage _in_reply_to = 0;

    Channel(const nlohmann::json& data);
    ~Channel();
    void update(const nlohmann::json& data);
    nlohmann::json dump() const;
    void commit(std::function<void (const bool)> cb = nullptr, CChannel source = nullptr);
    CGuild get_guild() const;
    __attribute__((pure)) std::string get_mention() const;
    void get_guild(std::function<void (CGuild)> cb) const;
    void send_json(nlohmann::json msg, std::function<void (CMessage)> cb = nullptr);
    void send(Message& msg, std::function<void (CMessage)> cb = nullptr);
    void send(const std::string& text, std::function<void (CMessage)> cb = nullptr, const nlohmann::json& embed = nullptr);
    void send_embed(const nlohmann::json& embed, const std::string& text = "", std::function<void (CMessage)> cb = nullptr);
    void edit_message(const uint64_t message_id, const std::string& text, std::function<void (CMessage)> cb = nullptr, const nlohmann::json& embed = nullptr, uint32_t flags = 0);
    bool has_perm(uint64_t uor_id, bool is_role, Permissions::type permission) const;
    bool has_perm(CUser user, Permissions::type permission) const;
    bool has_perm(Role *role, Permissions::type permission) const;
    void delete_message(uint64_t msg_id, std::function<void (const bool)> cb);
    void delete_message(uint64_t msg_id);
    void remove(std::function<void (const bool)> cb = nullptr);
    void _pins_cache_invalidate();
    void get_pins(std::function<void (const bool, std::unordered_map<uint64_t, CMessage>&)> cb);
    void pin_message(uint64_t msg_id, std::function<void (const bool)> cb = nullptr);
    void pin_message(CMessage msg, std::function<void (const bool)> cb = nullptr);
    void unpin_message(uint64_t msg_id, std::function<void (const bool)> cb = nullptr);
    void unpin_message(CMessage msg, std::function<void (const bool)> cb = nullptr);
    void create_invite(std::function<void (std::optional<Invite>)> cb = nullptr, bool temporary = false, bool unique = true, uint32_t max_uses = 0, uint32_t max_age = 0);
    void get_invites(std::function<void (std::optional<std::vector<Invite>>)> cb);
    void start_typing(std::function<void (const bool)> cb = nullptr);
};
}

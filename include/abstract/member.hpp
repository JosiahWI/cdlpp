#pragma once
namespace CDL {
class Member;
}

#include <string>
#include <vector>
#include <optional>
#include <nlohmann/json.hpp>
#include "../cdltypes-incomplete.hpp"


namespace CDL {
// https://discord.com/developers/docs/resources/guild#modify-guild-member
struct VoiceState {
    std::optional<bool> mute, deaf;
    uint64_t channel_id = 0;
};

// https://discord.com/developers/docs/resources/guild#guild-member-object
class Member {
public:
    std::string nick;
    bool deaf,
         mute;
    uint64_t user_id;
    CUser lazy_user;
    CGuild guild;
    std::vector<uint64_t> roles;

    Member(const nlohmann::json& data, uint64_t user_id, CGuild guild, CUser user = nullptr);
    void update(const nlohmann::json& data, uint64_t user_id, CGuild guild, CUser user = nullptr);
    nlohmann::json dump() const;
    void commit(std::function<void (const bool)> cb = nullptr, CMember source = nullptr);
    void get_user(std::function<void (CUser)> cb);
    void get_user(std::function<void (CUser)> cb) const;
    void set_nick(const std::string& new_nick, std::function<void (const bool)> cb = nullptr);
    void ban(const std::string& reason = "", uint16_t delete_message_days = 0, std::function<void (const bool error)> cb = nullptr);
    void kick(const std::string& reason = "", std::function<void (const bool error)> cb = nullptr);
    void set_voice_state(VoiceState voiceState, std::function<void (const bool)> cb = nullptr);
};
}

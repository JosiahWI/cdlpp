#include <memory>
#include "abstract/s2i.hpp"

namespace CDL {
class Channel;
class Message;
class User;
class Emoji;
class Guild;
class Member;
class Role;
class Ban;
class Presence;
class Invite;
class Reaction;

using CChannel = std::shared_ptr<Channel>;
using CMessage  = std::shared_ptr<Message>;
using CMember = std::shared_ptr<Member>;
using CUser = std::shared_ptr<User>;
using CEmoji = std::shared_ptr<Emoji>;
using CGuild = std::shared_ptr<Guild>;
using const_CChannel = std::shared_ptr<const Channel>;
using const_CMessage = std::shared_ptr<const Message>;
using const_CMember = std::shared_ptr<const Member>;
using const_CUser = std::shared_ptr<const User>;
using const_CEmoji = std::shared_ptr<const Emoji>;
using const_CGuild = std::shared_ptr<const Guild>;
};
